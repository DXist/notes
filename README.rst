Notes
=====

* `PostgreSQL Consulting seminar notes <https://bitbucket.org/DXist/notes/src/master/pgseminar.rst>`_
* `YaC 2014 <https://bitbucket.org/DXist/notes/src/master/yac2014.rst>`_
* `First Moscow Docker Meetup <https://bitbucket.org/DXist/notes/src/master/dockermoscow1.rst>`_
* `Dive into NuoDB <https://bitbucket.org/DXist/notes/src/master/nuodb.rst>`_
* `Moscow Python.Conf <https://bitbucket.org/DXist/notes/src/master/moscowpython2016.rst>`_
