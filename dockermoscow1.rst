Docker meetup
=============

Use cases
---------

* Separation
* Isolation
* Fast: sandboxes
* Easy: a lot of ready images
* Repeatable: CI & test

Ecosystem
---------
* Shipyard and other UIs
* orchestration tools
* communtity

  * 40000 + trained devops


Docker off the grid by Matthew Mosesohn
---------------------------------------

* Docker images

  * base image
  * additional image layers
  * metadata

* docker pull

  * have some risks

    * external service - connectivity, security, docker hub shutdown

    * who concerned

      * proprietary code
      * who do CI/CD and need performance

* how to transport

  * system packages
  * tarball via network protocol

    * save several images to on tar
    * lrzip compression - faster tha xz, better compression than gzip/bzip2
    * docker-squash to save time

  * Docker registry

    * uncompressed, but local
    * deployed as a container
    * pros

      * centralied
      * lightweight

      * native

    * cons

      * not highly available

* build centrally or on demand


Scaling the Docker Registry by Denis Zaitsev
--------------------------------------------

* используют в PaaS
* registry похож на репозиторий пакетов

  * docker daemon запускать со своим registry

* docker-registry поддерживает storage drivers

  * file
  * S3
  * other - Elliptics

    * желательна поддержка кеширования


Murano каталог приложений
-------------------------

* пользователи могут быть без тех бекграунда
* возможности

  * публиковать
  * управлять доступом
  * составлять окружения из нескольких приложений
  * управление жизненным циклом

* ключевые возможности

  * создание/обновление стеков приложений
  * динамический интерфейс создания приложения
  * workflow для управления жизненным циклом

    * actions

      * HA
      * autoscaling

        * связь с мониторингом
          * ceilometer

  * поддержка нескольких видов пакетов приложений

    * HOT Templates
    * Murano PL


Libcontainer: joining forces under one roof Andrey Vagin
--------------------------------------------------------

* history

  * Parallel Virtuozzo Containers
  * Linux VServer
  * OpenVZ
  * LXC
  * Linux-utils (unshare, nsenter)
  * SystemD (systemd-nspawn)
  * Libcontainer (Docker)

* cgroups

  * cpu, memory, blkio, freeze, ...

* namespaces

  * mnt, pid, net, ipc, user, uts

* libcontainer - чтобы не зависеть от внешнего проекта
* libct - для поддержки OpenVZ ядра
