Заметки Moscow Python.Conf
=========================

* Инструментирование кода

   * sys.settrace / threading.settrace

* удалённая отладка

   * rpdb
   * trepan2

* метапрограммирование в closure

   * homoiconity - ast == код

* Universal Scalability Law
* Методика тестирования
  * Smoke
   * Max performance
   * на разладку
   * тайминги
   * утечки
   * автоматизация и регрессия
