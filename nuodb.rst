Dive into NuoDB
===============

* NewSQL. It’s a true SQL service: all the properties of ACID transactions,
  standard SQL language support and relational logic.

* 3 tier architecture

  * administrative tier
  * transactional tier - Transaction Engine, TE

    * has local cache

  * storage tier - Storage Manager, SM


Transactional tier
------------------

* SQL frontend - maps SQL to internal representation

* internal structure: all data associated with a database, including the
  internal meta-data is represented through an Atom

  * Atoms are active network peers that can be cached, marshalled and
    unmarshalled

    * Catalog Atoms

      * resolve other Atoms across the running processes
      * handled in the same way as database atoms - good for consictency

* MVCC

  * TEs are caches, store multiple versions of data
  * Optimistic updates

    * updates can be communicated optimistically, because a rollback is done by
      simply dropping a pending update from the cache.

    * messages are also sent asynchronously, allowing a transaction to proceed assuming that an update will succeed
    * Asynchrony within a transaction can mask network round-trip time

  * Visibility Model

    * Snapshot Isolation - default model

      * provides a consistent view of the database from the moment that a
        transaction started

      * conflict mediaion:
      * On update or delete NuoDB chooses a Transaction Engine where the Atom
        resides to act as tiebreaker - the Chairman
      * for each Atom there is a known TE playing this role
      * Only TEs that have a given object in their cache can act as Chairman for that object, so in
        the case where an object is only cached in one TE all mediation is
        local.
      * When a TE shuts down, fails or drops an object from its cache
        there’s a known way to pick the next Chairman with no communications
        overhead.

Data durability
---------------

* Storage

  * access and caching is on the Atom-level
  * Atom has unique ID
  * Atoms are stored as key-value pairs

* Each SM adresses full copy of the database

  * optionally maintain a journal of all updates


Tunable commit protocol
-----------------------

* TE have to coordinate with SM
* commit protocol

  * tradeoff between performance and durability concerns
  * change in transaction context propagates to all related parties - SMs and
    Atom Chairmen (TE with the Atom in-cache)

    * k-safety at TE and eventual durability for SM

  * if it's not enough

    * acknowledge commit after data has become durable on at least N SM
      (Remote:N)
    * acknowledge when the change has been written to the journal

Data update
-----------

* Atom is cached in the Chairman TE1 and replicated to TE2
* 2 messages about update

  * permission from the Chairman
  * pending updates to peers with the Atom copy

* conflict - 2 TEs want to update the same data (row in the table)

  * Chairman: first transaction wins


Management tier
---------------

* collection of peer processes - Agents

  * run on all hosts where the database could be active - Management Domain

* Agent is responsible for local TE and SM
* Broker - is additional role of Agent

  * load balancing point
  * SQL client connects to a Broker
  * Broker tell the client the TE to use

* Logical Administration

  * databases can be started from templates - recipes solving common problems:

    * redundancy, scaling out, running in multiple locations

* Backup

  * full & incremental - is done from redundant SM, which is shut down


Pros & Cons
-----------

* Geo Distribution - cache and coordination are local - close to data usage
* Flexible Schemas - schema change operations are done in constant time
* OLTP and Analytic mixed workloads because of MVCC, dedicated TE for different
  tasks
