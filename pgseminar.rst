PostgreSQL
==========
+ самая безопасная СУБД

Термины
=======

* Граф конфликтов
* Гранулярность блокирок
* Two Phase Locking 2PL

  * нужно бороться с дедлоками - deadlock детектор
    wait for graph - граф ожиданий

  * пессимистичный scheduler
* оптимистичный scheduler

  * validate commit
  * commit
  * Galera - распределённая система

* bloat - заполненение страниц мертвыми tuple

  * HOT-update - heap only tuple - не живёт в индексе - оптимизированный update

    * fillfactor - при большом кол-ве update'ов

  * pg compact table

  * Загрузка больших объёмов

    * нативными средствами - copy
* freeze - закончился 32 битный id транзакций
  * следить за autovacuum wraparound

Проблемы распределённых систем
==============================
* Синхронизация времени - подход с timestamp
* целостность на уровне приложения не обеспечить

MVCC
====
* плохо совмещать длинные и короткие запросы
* MV2PL

  * Mysql не перечитывает версии
  * Postgresql

    * подтягиваем новые версии из data-файла
    * UPDATE = INSERT + DELETE
    * DELETE - убирает из области видимости, хуже чем UPDATE
    * xmin, xmax - интервал видимости, показывает валидность транзакций

      * pg_clog - файл с масками - оптимизация проверок

    * Book: Transactional Information Systems

WAL
===
* сжато записывать грязную страницу

  * алгоритм восстановления транзакций ARIES, Mohan

* хорошо размещать на отдельном диске
* комбинируется с checkpoint'ами
* храним undo и redo информацию
  в Posgres - redo - в WAL, undo в датафайле
* восстановление

  * redo COMMITED winner
  * undo loosers - незавершенные

* WAL - 2-х уровневый кэш
* тюнинг записи

  * wal_buffers - на быстрых дисках - сразу несколько грязных страниц
  * fsync + refill кэша
  * checkpoint_segments
  * checkpoint_timeout - дольше восстановление, но слишко часто
  * checkpoint_completion_target - можно сгладить пик IO - 0.9, 0.7
  * таблица pg_stat_bgwriter
  * bgwriter - в фоновом режиме пишет грязные страницы

    * может списывать по переполнению кеша
    * buffers_backend_fsync - checkpoint'ы наезжают друг на друга !
    * статистику можно сбрасывать каждую ночь
    * %util лучше iops - iostat
    * bgwriter_delay - пореже
    * bgwriter_lru_maxpages

  * barrier - ext4 / xfs

    * журнал хорошо работает на ноутбуке
    * но он перемешивается с блоками данных
    * syscall barrier - запрещает флаш - сортировка kernel buffer cache
    * при больших объёмах shared memory - barrier - узкое место

  * не нагружать посторонними задачами - checkpoint важен

PG каталог
==========
* сгруппирован по oid - id сущности в базе


Настройка БД
============
* предсказуемое latency к диску
* таблица pg_catalog.pg_settings
* maintenance_work_mem

  * create index concurrently

* пропускная способность дисков должна соответстовать кол-ву dirty страниц
* autovacuum

  * по умолчанию - недостаточно агрессиве

    * autovacuum_analyze_scale_factor
    * autovacuum_vacuum_scale_factor - до 0.001
    * ! идентификатор транзакций - 32 битный
    * freeze_max_age - на max - to prevent wraparound
    * max_workers - должны покрывать scale_factor
    * cost_delay - 10

  * pg_stat_activity, ps -aux - мониторинг
  * хорошо делать ionice

* connection pool - must have

  * pg_bouncer

RAID
====
* честный
* для производительности - backup battery unit
* 2.5" SAS
* enterprise SSD intel dc 3700
* дисковый массив на оптике

  * multipath - балансировка между дисками

Плохие диски
============
* покупать хорошие
* на плохих дисках можно synchronous_commit -> off
* больше воркеров автовакуума
* нельзя

  * commit_delay
  * commit_siblings

Tools
=====
* pgrework - дефрагментация
* pg_buffercache

  * профилирование buffer cache

* pg_bouncer - аккуратно с user переменными

  + libevent - лёгкий
  + 3 режима пула - transaction, statement, session

    * когда возвращаются коннекты
    * transaction режим - самый эффективный
      нужно отключить prepare statement

  - не нужно использовать сессионные переменные

  * через pgbouncer надо ходить только приложениям

  * можно объединить несколько pg_bouncer с помощью HAproxy

  + можно ставить на паузу - обновление Postgres

    * нужно вручную сделать checkpoint 2 раза

* pgpoolII

  - использует форки
  - statement based репликация - ненадёжная


Репликация
==========

* на репликацию нельзя положиться
* hotstandby (предпочтительнее) и всё остальное
* бекап

  * грязный файловый бекап + WAL

* hotstandby

  * wal_sender, wal_applyer
  * используем

    * для резерва
    * разгружаем master - tsearch
    * автоматический failover - плохо из-за ложных срабатываний

* master - master - это плохо (coordinator, slave, slave)

  * 2 phase commit

    * ready to commit
    * commit
    * долго
    * при потере master'a и 1 slave - всё
    * учащение восстановления

  * Oracle RAC - общий database
  * полноценного multi master нет
  * сравнение с hotstandby

    * всё равно нужно менять ip
    * приложение должно уметь обрабатывать транзакции - уметь работать с
      несколькими нодами.

* settings

  * hot_standby_feedback on - сообщает о грязных страницах,
    используемые долгими запросами на slave

Process
=======
* мониторинг
* просмотр топ медленных запросов

Масштабирование
===============
* scale back
* partitioning таблиц
* PLProxy - пишем func - интеграция на чтение - autocommit
* dblink соединение к другому Postgres

Производительность
==================
* нельзя мерить эмпирически
* split-тестирование с реального трафика
* pgbench
* pg_test_fsync
* время запроса

  * передача клиент -> сервер
  * парсинг
  * оптимизация
  * исполнение
  * возврат рез-в

* виды оптимизаторов

  * rule based
  * cost based

* оптимизация запросов

  * explain (analyze on, buffers on)
  * dellstore - тестовая база
  * сложная задача
  * можно получить n! вариантов плана при join n таблиц
  * оптимизатор не всемогущий

    * проверить - собрана ли статистика

  * count - очень часто не нужен - seq scan - требуется проверять версию
    можно взять приближённое значение из pg_catalog
  * понять смысл запроса

* shared_buffers

  * не слишком много (до 25%) - если база не влазит в память
  * иначе можно до 75%

* прогрев

  * для проблемных запросов - можно искусственно

* IN плох
* генерация запросов

  * может помочь PREPARE - но не всегда
  * можно достать план по sql id
  * плох при pgbouncer

* алгоритм join

  * Nested loop
  * Merge join
  * Hash join

* поднятие workmem - надёжно в хранимой процедуре
* написание хороших SQL

  * проверяем пока пишем
  * наблюдаем за запросом

* generate series - генерит dataset - хотя бы 999999 записей
* поиск проблем

  * глупые проблемы

    * кто менял конфиги, обновляли пакеты, подцепился не тот конфиг
    * per table settings
    * table_write_activity.sql
    * indexes_with_nulls - null хранится эффективно

  * top проблемных запросов

    * loganalyze

      * COMMIT - проблема с fsync

    * pgstatstatements

Хранимые процедуры
==================
* зачем

  + экономия на многократных хопах до базы
  + управление транзакциями

* что не делать

  * генерировать запросы
  * писать хранимки на не PL/pgSQL

    * runtime загружается в workmem

* неплохо использовать PL/R
* foreign data wrapper file
* аттрибуты PL/pgSQL функций влияют на производительность

  * функциональный индекс должен строится по IMMUTABLE функциям
  * можно контролировать доступ

* можно использовать Exceptions

  * возврат будет до неявного savepoint, созданного при входе в scope


Common Table Expressions
========================
* эквивалент вложенному запрос - создаёт временную таблицу - в рамках транзакции
* удобство
* рекурсивный запрос

Window functions
================
* можно перемещаться окном по dataset'у и что-нибудь делать
* полезно для аналитических отчётов

  * row_number - монотонный номер

Constraints
===========
* единственный критерий оценки верности введённых данных

  * EXCLUDE - запрещение пересекающихся интервалов
  * DEFERRABLE / NOT DEFERRABLE - отложенные до конца транзакции constraints

Блокировки
==========
* advisory locks - рекомендательные

  - обычно база сама может разрулить за счёт транзакций

* обычные блокировки - select for update

  * nowait - выдаёт ошибку вместо блокирования

Продвинутые возможности
=======================
* extension

  * mbus - реализация очереди, pub/sub

Советы
======
* kill запросов по cron
* скрипты миграций - вручную

Ресурсы
=======
* sql.ru, раздел PostgreSQL
* maillist postgresql - hackers, performance
* книги

  * High Performance PostgresSQL
  * Administering PostgreSQL
